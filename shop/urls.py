from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^list/$', views.productList.as_view(), name='list'),
    url(r'^list/(?P<category_slug>[-\w]+)/$', views.productList.as_view(), name='list_by_cate'),
    url(r'^detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.productDetail.as_view(), name='detail'),
]
