from django.shortcuts import render, get_object_or_404
from django.views import View
from cart.forms import CartAddProductForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import Category, Product
# Create your views here.

class productList(View, LoginRequiredMixin):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'

    @method_decorator(login_required)
    def get(self, request, category_slug=None):
        category = None
        categories = Category.objects.all()
        products = Product.objects.filter(available=True)
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            products = products.filter(category=category)
        return render(request, 'shop/product/list.html', {'category': category,
                                                            'categories': categories,
                                                            'products': products,
                                                            'section': 'tienda'})
class productDetail(View):

    def get(self, request, id, slug):
        product = get_object_or_404(Product, id=id, slug=slug, available=True)
        cart_product_form = CartAddProductForm()
        return render(request, 'shop/product/detail.html', {'product': product, 'section':'tienda', 'cart_product_form':cart_product_form})
