from django.db import models
from django.core.urlresolvers import reverse
# Create your models here.
class Category(models.Model):
    name = models.CharField('Nombre de categoria', max_length=200, db_index=True)
    slug = models.SlugField('Slug', max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name', )
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:list_by_cate', args=[self.slug])

class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products')
    name = models.CharField('Nombre', max_length=200, db_index=True)
    slug = models.SlugField('Slug', max_length=200, db_index=True)
    image = models.ImageField('Imagen', upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField('Descripción', blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveSmallIntegerField()
    available = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name', )
        index_together = (('id', 'slug'), )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:detail', args=[self.id, self.slug])
